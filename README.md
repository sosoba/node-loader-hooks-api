# Typescript definitions for Node loader hooks

### Based on https://nodejs.org/dist/latest-v18.x/docs/api/esm.html#loaders

### Ussage:

```sh
npm i -D node-loader-hooks-api
```

`loader.ts`
```ts

import type {Loader, Resolver} from 'node-loader-hooks-api';

export const resolve: Resolver = async(specifier, context, defaultResolve) => {
  // ... your custom resolver
  return defaultResolve(specifier, context, defaultResolve);
}

export const load: Loader = async(url, context, defaultLoad) => {
  // ... your custom loader
  return defaultLoad(url, context, defaultLoad);
};
```