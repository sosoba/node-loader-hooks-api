export type Context<A> = {
  conditions: string[];
  importAssertions: A;
  parentURL?: string | undefined;
  format?: string | null | undefined;
};

export type Resolver<A = unknown> = (
  specifier: string,
  context: Context<A>,
  nextResolve: Resolver,
) => Promise<{
  format?: string | 'builtin' | 'commonjs' | 'json' | 'module' | 'wasm' | null | undefined;
  shortCircuit?: boolean | undefined;
  url: string;
}>;

export type Loader<A = unknown> = (
  url: string,
  context: Context<A>,
  nextLoad: Loader,
) => Promise<{
  format: string;
  shortCircuit?: boolean | undefined;
  source: ArrayBuffer | string;
}>;
